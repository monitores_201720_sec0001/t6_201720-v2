package view;

import java.io.IOException;
import java.util.Scanner;

import model.exceptions.TripNotFoundException;
import controller.Controller;

public class STSManagerView {

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					Controller.loadStopTimes();
					
					break;
				case 2:
					
					System.out.println("Ingrese el id de la parada por ejemplo 646 :");
					Integer stopId =sc.nextInt();
				    System.out.println(Controller.darViajesDeLaParada(stopId));
					
					break;
				case 3:
					System.out.println("Ingrese el id del viaje 1:");
					Integer tripId1 = sc.nextInt();
					System.out.println("Ingrese el id del viaje 2:");
					Integer tripId2 = sc.nextInt();
					System.out.println(Controller.darParadasCompartidas(tripId1, tripId2));
				
				try {
					Controller.listStops(tripId);
				} catch (TripNotFoundException e) {
					System.out.println("El id ingresado no existe");
				}
					break;
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("1.Cargue la informacion necesaria");
		System.out.println("2. Dar los viajes de la parada");
		System.out.println("3. Dar paradas de un viaje");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
	}
}
