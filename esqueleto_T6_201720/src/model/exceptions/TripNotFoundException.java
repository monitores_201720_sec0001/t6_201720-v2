package model.exceptions;

public class TripNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TripNotFoundException(){
		super("The trip id is not valid");
	}
}
