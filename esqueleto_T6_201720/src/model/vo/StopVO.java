package model.vo;

public class StopVO {

	private Number RouteNo;
	private String RouteName;
	private String Direction;

	private String Pattern;
	private String Destination;
	private String ExpectedLeaveTime;
	private double ExpectedCountdown;
	
	private String ScheduleStatus;
	private boolean CancelledTrip;
	private boolean CancelledStop;
	private boolean AddedTrip;
	private boolean AddedStop;
	private String LastUpdate;
	
	public Number getRouteNo() {
		return RouteNo;
	}
	public void setRouteNo(Number routeNo) {
		RouteNo = routeNo;
	}
	public String getRouteName() {
		return RouteName;
	}
	public void setRouteName(String routeName) {
		RouteName = routeName;
	}
	public String getDirection() {
		return Direction;
	}
	public void setDirection(String direction) {
		Direction = direction;
	}

	public String getPattern() {
		return Pattern;
	}
	public void setPattern(String pattern) {
		Pattern = pattern;
	}
	public String getDestination() {
		return Destination;
	}
	public void setDestination(String destination) {
		Destination = destination;
	}
	public String getExpectedLeaveTime() {
		return ExpectedLeaveTime;
	}
	public void setExpectedLeaveTime(String expectedLeaveTime) {
		ExpectedLeaveTime = expectedLeaveTime;
	}
	public double getExpectedCountdown() {
		return ExpectedCountdown;
	}
	public void setExpectedCountdown(double expectedCountdown) {
		ExpectedCountdown = expectedCountdown;
	}
	public String getScheduleStatus() {
		return ScheduleStatus;
	}
	public void setScheduleStatus(String scheduleStatus) {
		ScheduleStatus = scheduleStatus;
	}
	public boolean isCancelledTrip() {
		return CancelledTrip;
	}
	public void setCancelledTrip(boolean cancelledTrip) {
		CancelledTrip = cancelledTrip;
	}
	public boolean isCancelledStop() {
		return CancelledStop;
	}
	public void setCancelledStop(boolean cancelledStop) {
		CancelledStop = cancelledStop;
	}
	public boolean isAddedTrip() {
		return AddedTrip;
	}
	public void setAddedTrip(boolean addedTrip) {
		AddedTrip = addedTrip;
	}
	public boolean isAddedStop() {
		return AddedStop;
	}
	public void setAddedStop(boolean addedStop) {
		AddedStop = addedStop;
	}
	public String getLastUpdate() {
		return LastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		LastUpdate = lastUpdate;
	}
	
}
