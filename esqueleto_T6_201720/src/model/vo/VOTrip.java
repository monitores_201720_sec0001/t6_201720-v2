
package model.vo;

import java.util.Date;

public class VOTrip {

	
	private int trip_id;

	private Date tiempoDeLlegada;
	

	public VOTrip(int pTripID, Date pTiempoDeLlegada){

		trip_id=pTripID;
		
		tiempoDeLlegada=pTiempoDeLlegada;
	}
	

	public Date darTiempoDeLlegada(){
		return tiempoDeLlegada;
	}
	

	public int darTripId(){
		return trip_id;
	}


}
