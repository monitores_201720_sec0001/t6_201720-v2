package model.vo;

public class VOStopTimes {

	private int trip_id;
	private String arrival_time;
	private String departure_time;
	private int stop_id;
	private int stop_sequence;
	private int stop_headsign;
	private int pickup_type;
	private int drop_off_type;
	private int shape_dist_traveled;
	private arraylist paradas;

public VOStopTimes(int pTrip_id,String pArrival_time,String pDeparture_time,int pStop_id,int pStop_sequence,int pStop_headsign,int pPickup_type,int pDrop_off_type,int pShape_dist_traveled){
	trip_id=pTrip_id;
	arrival_time=pArrival_time;
	departure_time=pDeparture_time;
	stop_id=pStop_id;
	stop_sequence=pStop_sequence;
	stop_headsign=pStop_headsign;
	pickup_type=pPickup_type;
	drop_off_type=pDrop_off_type;
	shape_dist_traveled=pShape_dist_traveled;
}
public int getTrip_id() {
	return trip_id;
}

public String getArrival_time() {
	return arrival_time;
}


public String getDeparture_time() {
	return departure_time;
}

public int getStop_id() {
	return stop_id;
}


public int getStop_sequence() {
	return stop_sequence;
}


public int getStop_headsign() {
	return stop_headsign;
}


public int getPickup_type() {
	return pickup_type;
}



public int getDrop_off_type() {
	return drop_off_type;
}



public int getShape_dist_traveled() {
	return shape_dist_traveled;
}


}
